from collections import OrderedDict
import re



#whitelisted set of acceptable files. (avoid injections)
DATA_FILE_MAP = { 'normal':'./data/normal.csv','advanced':'./data/advanced.csv'}

#default correlation value for users to match
CORRELATION_FLOOR = 0.7

#Where generated JSON files are stored
JSON_FILE_PATH_FORMAT = "./output/{name}.json"




def force_padded_zip(x):
    """Force param to be a string rep of a padded integer

    Note - this is a little clugey way to handle how zips are cleaned.

     post-prototype it could me made more generic"""
    if not x:
        return '00000'

    if isinstance(x, int) or isinstance(x, float):
        z = "{0:05d}".format(int(x))
        return z

    #if unknown type return a default
    return '00000'


def numeric_only(s):
    """ Remove all non numeric chars from string"""
    return re.sub(r'[^0-9 ]', '', s)





