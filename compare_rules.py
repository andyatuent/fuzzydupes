from collections import OrderedDict
from fuzzywuzzy import fuzz
from common import force_padded_zip, numeric_only




#####
# The column meta contains the rules and functions on how to clean and compare each of the columns.
# Approach
#    __DEFAULT_COL_META contaons all defaults for every rule.
#    __COL_META contains column specific over-rides.

#tech notes:
# look at how last_name uses a different Levenshtein function to handle potential differences in last names.



#these are the default values for ech column in the __COL_META.
__DEFAULT_COL_META = {'ignore': False, #if True - there is no processing or comparison of this column
                      'default_val':'',
                      'case_sensitive': False,# True if values are case sensitive.   (oif False all comares are case insentive
                      'whitespace_significant': False,#True if whoitespace should be left in comparisons
                      'weight':1, #int.  How much this column's correlation impacts overal correlation. Higher is better
                      'func_clean_pre': None,      #python function to call before other cleaning ops.   Optional.
                      'func_clean_post': None,      #python function to call after other cleaning ops.   Optional.
                      'func_compare':fuzz.ratio}    #pythion function to use for comparison.   Must have syntax F(x,y)

#Note that comparisions default to simple Levenshtein  ratio (fuzz.ratio)
# Other comparison algorithms can be tested by overriding column settings in __COL_META

#information and rules about how to handle each column in raw and derived data.
__COL_META = {
    'id':      {'ignore': True},
    'user_id': {'ignore': True},
    'first_name': {'weight': 2},   #weight is lower. First names could be nicknames.  TODO consider dictionary with common nicknames.
    'last_name':  {'weight': 5, 'func_compare':fuzz.partial_ratio}, #use partial_ratio() to smooth out puncuation in ast names.
    'company':    {'weight': 1},
    'email':      {'weight': 8},
    'email_root': {'weight': 3, 'ignore':True}, #placeholder for future improvement
    'address1':   {'weight': 4},
    'address2':   {'weight': 1},
    'zip':        {'weight': 3, 'default_val':'00000', 'func_clean_pre':force_padded_zip},
    'city':       {'weight': 2},
    'state':      {'weight': 3, 'default_val': 'XX'},
    'state_long': {'weight': 1},
    'phone':      {'weight': 10,  'func_clean_pre': numeric_only},
}





def get_column_rules():
    """
    Return column metadata - how to compare, parse, etc.
    """
    #generate a dict with all defaults
    #overwrite with actual

    meta = OrderedDict()
    for k,v in __COL_META.items():
        meta[k] = dict(__DEFAULT_COL_META)
        meta[k].update(__COL_META[k])

    return meta





