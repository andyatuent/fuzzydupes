"""

Fetch user duplicate JSON from fuzzy service.   Print output to the console


Note:  URLlib library does NOT validate certs for HTTPS.   This must be done as separate step.   See certfi package.
"""

import urllib.request
import socket
import json
import logging
import xml.etree.ElementTree as ET

#Standard logging formats.  Goal is consistent formats to allow machine parsing.
URL_STD_FORMAT = 'URL[{url}];code[{status}] '

JSON_MIME   = 'application/json'


def format_exception(errcode, errmsg):
    """ Utility to make sure exceptions have common format and and are parsable.
    :param errcode:   Integer
    :param errmsg:    Human readable message.     Caller may wish to format in CSV.
    :return: formatted string.
    """



def fetch(url, exp_ret_code = 200, headers = {}):
    """
    http helper.  Fetch URL and return payload.  This routine handles common error checking,
    logging and some auto correction.

    The intention is to handle common HTTP request errors in one reusable function.
    :param url:    Full URL to fetch.
    :param headers: dictionary of default request headers.   These will override any supplied
    :param exp_ret_code:  expected return code.    Its an error if response code is anything else.
    :return:  HTTPresponse object containing payload.  If None it means request was not successful. See log.
    :todo:: See code note about adding re-authentication and re-try.
    :example:

    res = fetch('http://127.0.0.1:5000/index.json')
    if res:
        parse payload.
    """

    ret_resp = None


    try:
        #simple - OK.
        #req  = urllib.request()
        #resp = req.urlopen(url=URL, headers={})

        #f = req.opener.open(req)
        #json = json.loads(f.read())
        #print(json)
        #print(json['unit'])

        req = urllib.request.Request(url=url, headers=headers, method='GET')
        res = urllib.request.urlopen(req)

        # - Performance note:  avoid building big error or log buffers for positive-cases
        if res is None:
            #note - URLLIB already handles most common errors and already throws execptions when noral bad HTTP things hapen.
            #handle case where it doesn't.  This implies some bad internal error with client's environment
            # We would normally throw some custome exception  (rather than response code) to indicate something is bad with cloient.
            raise Exception(format_exception(errcode=100, errmsg="Internal error.   None returned when accessing URL[url]".format(url=url)))

        logging.info( (URL_STD_FORMAT +  "Top").format(url=url, status=res.status, l=res.length))

        #handle normal HTTP failure conditions.  i.e., request was successful from HTTP level, but maybe not for app.
        if 200 is not res.status:
            logging.error((URL_STD_FORMAT +  " - status <> 200.    Error").format(url=url, status=res.status))
            #TODO - add checks to see if error is correctable at this level.    Redirects, re-authenticate, etc.
            return None


        #successful request.
        ret_resp = res

    except urllib.error.HTTPError as e:
        #TODO Handle HTTPError exception - either retry or report failure.
        logging.error("TODO - Handle HTTPError exception.    ")
        raise e   #rethrow so we dont forget.

    except socket.timeout:
        #TODO Handle timeout exception - either retry or report failure.
        print("Handle timeout exception.    ")

    except:  #Unexpected exception.     Non-recoverable.  log error and ret.
        raise   #rethrow - caller will need to handle.


    return ret_resp


def count_lines_json(j):
    """return the number of lines in json text.

    :param j: string buffer containing JSON.
    :return: int of lines.  returns 0 if empty or invalid
    """
    if not j:
        return 0

    #deserialize into a py data structure
    #Note - this may not be apprproate for large payloads.
    p = json.loads(j)

    logging.debug("json payload is of py type[{}]".format(type(p)))
    num_lines = len(p)

    return num_lines


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    headers = {
        'allow_redirects':False,
        'Accept':JSON_MIME,
    }

    data='normal'
    corr = 0.75
    url = 'http://0.0.0.0:8081/?data={data}&correlation_floor={corr}'.format(data=data, corr=corr)

    res = fetch(url, headers=headers)
    if res:  #process if successful.

        mime = res.getheader('Content-type')
        if mime == JSON_MIME:
            x = count_lines_json(res.read())




    debug_tgt = 12