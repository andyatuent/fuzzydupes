

import pandas as pd
import csv
import logging
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

#if match confidence is greater or equal to this value then the corrected value is returned.
CONFIDENCE_FLOOR = 0.8

# labels used to index CSV nd dictionaries
# TODO prototype - these must match csv.
ZIP = 'Zip Code'
PLACE = 'Place Name'
STATE = 'State'
STATE_CODE = 'State Abbreviation'
COUNTY = 'County'

class AddressCheck:

    rows_dict=None
    zip_dict=None

    def __init__(self):
        self.reset()

    def reset(self):
        """Reset internal state"""
        self.zip_dict = {}
        self.rows_dict = []

    def from_csv(self, csv_path):
        """Load and prepare City data from external CSV

        :param full path to CSV with city data
        NOTE:    PROTOTYPE - only works with specifc CSV format.   See labels in code"""

        self.reset()

        # TODO prototype - let exceptions through to caller

        with open(csv_path) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                d =   {ZIP:  "{0:05d}".format(int(row[ZIP])),
                       PLACE:row[PLACE].lower().strip(),
                       STATE:row[STATE].lower().strip(),
                       STATE_CODE:row[STATE_CODE].lower().strip()
                }
                self.rows_dict.append(d)
                self.zip_dict[d[ZIP]] = d    #index by zip code
        logging.info("loaded [{n}] cities".format(n=len(self.rows_dict)))

    def fix(self, addr, confidence_floor = CONFIDENCE_FLOOR):
        """Return corrected address tuple.

        :param addr - address to check
        :param confidence_floor - value between 0 and 1.   if actual comparision confidence is > confidence_floor then consider it a matgch and return corrected value.

        :returns tuple (fixed_addr, confidence)
        """

        confidence = 0
        corrected = False
        #look up by zip
        x = addr[ZIP]
        actual = self.zip_dict.get(addr[ZIP])

        #TODO Handle case where zip was not found

        #Do State and place have an exact match
        if (addr[ZIP]        == actual[ZIP] and
            addr[STATE_CODE] == actual[STATE_CODE] and
            addr[PLACE]      == actual[PLACE]):

            confidence = 1.0
            #no change to addr
        else:

            #notes on weights - we use state code and zip as most indicitave.
            cols    = [ZIP, STATE_CODE, PLACE]
            weights = [  2,    3,          1]


            # tuple = column name & weight.
            cols = [   {'col': ZIP, 'weight':2}, {'col': STATE_CODE, 'weight':3}, {'col': PLACE, 'weight':1}]
            confidence = 1.0
            for col in cols:
                c = fuzz.ratio(addr[col['col']], actual[col['col']])
                confidence = confidence * ((c / 100.0) ** col['weight'])

            if confidence >= confidence_floor:
                logging.info("corrected city from [{}] to [{}] confidence[{}]".format(addr, actual, confidence))
                addr = actual
                corrected = True

        return (addr, confidence, corrected)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    addr = AddressCheck()
    addr.from_csv('./data/us_postal_codes.csv')

    #note test data have been normalized to lower
    good = {ZIP: '01505',PLACE:'boylston',STATE:'massachusetts', STATE_CODE:'ma'}
    bad_city = {ZIP: '01505',PLACE:'xxx', STATE:'massachusetts', STATE_CODE:'ma'}
    ok_city = {ZIP: '01505',PLACE:'boylsn', STATE:'massachusetts', STATE_CODE:'ma'}


    (updated, confidence, corrected)  = addr.fix(good)
    (updated, confidence, corrected)  = addr.fix(bad_city)
    (updated, confidence, corrected)  = addr.fix(ok_city)

    dbg = 13