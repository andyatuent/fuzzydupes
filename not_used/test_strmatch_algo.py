

"""test Levenshteihn algorithm through fuzzywuzzy

"""
from fuzzywuzzy import fuzz
from fuzzywuzzy import process



def normalize_remove_whitespace(arr):
    return [item.strip() for item in arr]

def normalize_lower(arr):
    return [item.lower() for item in arr]

if __name__ == "__main__":



    x = fuzz.ratio("this is a test", "this is a test!")
    y = fuzz.ratio("this is a test!", "this is a test")

    dbg = 12