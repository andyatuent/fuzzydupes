"""
Simple Flask server to allow fetch and process of sample csv files as JSON

To use:
    GET <server>?data=normal
"""
from flask import Flask, Response, request, redirect, flash, url_for, render_template
import time
from fuzzy_main import process, load
import pandas as pd
from fuzzy_main import *
import time
from common import *


JSON_MIME  = 'application/json'
app = Flask(__name__)

@app.route("/", methods=['GET'])
def fetch():

    data   = request.args.get('data')
    #TODO postprotype - add friendlier error checking

    if not data: #display default index page
        return render_template('index.html', err_msg=None)

    # This whitelist also prevents injection.
    # .   it only should be normal or advanced for prototype.
    src_file = DATA_FILE_MAP.get(data)
    if not src_file:
        return render_template('index.html', err_msg='data query parm must be one of normal or advanced')

    #load previously generated data
    res_dict = load_prior(data)
    if not res_dict:
        #this means JSON file was not found
        return render_template('index.html', err_msg='data query parm must be one of normal or advanced')

    return render_template('dupe_display.html',
                               res_dict=res_dict)

def load_prior(data_name):
    """
    Load a previously geneated JSON file into a py dictionary.

    :param data_name:  either normal or advanced.
    :return:
    """

    try:
        json_file_path = JSON_FILE_PATH_FORMAT.format(name = data_name)
        with open(json_file_path, "r") as f:
            j = json.load(f)
    except FileNotFoundError:
        #if FileNorFound then set to None to let caller know.
        # all other excptions are just passed through.
        j = None

    return j


def default_to(val, default=None):
    """helper.  Return default if value is None"""
    return val if val else default





if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8081)
