
# Overview of Approach
 
 * fuzzy_main.py - reads csv (from data directory), finds duplicate users and generates JSON files in output directory
 * flask_main.py - flask server.   run and go to root page for generated instructions. 
 
 * User Comparision is driven by ruleset  (see RULE SET below) and defined in compare_rules.py.   Also see RULE SET below. 
   * The main algorithm compares each column in the csv matrix to come up with a composite correlation score.
   * The important of each column in the final correlation score is weighted.
   * weights are guesses at this point,   Some true testing is needed.   (But the dynamic approach would allow more dynamic testing)
   * If two users have a calculated correlation score that is greater than the 
   CORRELATION_FLOOR the users are tagged as being duplicates.  The default CORRELATION_FLOOR is 0.7, but this is adjustable.
    
    
    
   * Correlation floor Analysis - How does correlation floor setting effect duplicate matching?
   
   Suite spot seems to be around .6 - .7.   To tne algorithm, consider testing weights on specifc columns. 
   
   Normal:
  
    | setting | # Dupes | Notes    (ids)          |
    |---------|---------|-------------------------|
    |   0.9   |     6   |  4,11,15,19,38,54       |    
    |   0.8   |     7   |  4,11,15,19,38,54,96    |
    |   0.7   |     7   |  4,11,15,19,38,54,96    |   
    |   0.6   |     8   |  Add user: Stanfield Rown dup. (id 66)   See note below.       |
    
    
    At corrrelation_floor = 0.7 we pick up these matches. 
    dict_values([66, 'stanfield', 'rown', 'reilly-champlin', 'srown1t@loc.gov', '3460 luster junction', '', '77005', 'houston', 'texas', 'tx', '2146935061', 72, 1.0])
dict_values([66, 'stanfield', 'rown', 'reilly-champlin company', 'srown1t@loc.gov', '', '', '00000', '', '', 'xx', '2146935061', 73, 0.73725]) Correlation=0.73725
  Notice that correltion id is 0.73 
     
  
    
    
   Advanced:
  
    | setting | # Dupes | Notes       (ids)       |
    |---------|---------|-------------------------|
    |   0.9   |     7   | 4,11,15,19,38,50,54     |    
    |   0.8   |     8   | 4,11,15,19,38,50,54,96  |
    |   0.7   |     8   | 4,11,15,19,38,50,54,96  |     
    |   0.6   |     9   | 4,11,15,19,38,50,54,66, 96 Add user: Stanfield Rown dup.  See note below.       |
    
    
    At corrrelation_floor = 0.7 we pick up these matches. 
    dict_values([66, 'stanfield', 'rown', 'reilly-champlin', 'srown1t@loc.gov', '3460 luster junction', '', '77005', 'houston', 'texas', 'tx', '2146935061', 72, 1.0])
dict_values([66, 'stanfield', 'rown', 'reilly-champlin company', 'srown1t@loc.gov', '', '', '00000', '', '', 'xx', '2146935061', 73, 0.73725]) Correlation=0.73725
  Notice that correltion id is 0.73 


   
 * Tech stack
    * python 3.6.   Look in code for TODO comments about some of fixups needed. 
      * example - there is nearly no exceptiojn  handled or error handling.    Except for FileNotFound I just let all exceptions flow back to caller. 
    * pandas (it made the cleaning and processing easier, but some of the row-oriented loops would not scale well.   Pandas is fast when accessed via columns, but slow when accessed as rows.) 
    * fuzzywuzzy - Levenshtein algorithms.  Note:  I keep the pure python version and did NOT install the cython version.   Cython would be faster, but I was most interested in portability. 
    * flask & Jinga2 - simple http server and templating. 

# RULE SET

Comparison is driven by per-column rules for cleaning and algorithm selection. 
We use Levenshtein from fuzzywuzzy.     The last_name column is compared via partial_ratio (to smooth out syntax and opuncuation).   All other colujmns are compared by default with simple ratoi()


   ```
#####
# The column meta contains the rules and functions on how to clean and compare each of the columns.
# Approach
#    __DEFAULT_COL_META contaons all defaults for every rule.
#    __COL_META contains column specific over-rides.

#tech notes:
# look at how last_name uses a different Levenshtein function to handle potential differences in last names.

#these are the default values for each column in the __COL_META.
__DEFAULT_COL_META = {'ignore': False, #if True - there is no processing or comparison of this column
                      'default_val':'',
                      'case_sensitive': False,# True if values are case sensitive.   (oif False all comares are case insentive
                      'whitespace_significant': False,#True if whoitespace should be left in comparisons
                      'weight':1, #int.  How much this column's correlation impacts overal correlation. Higher is better
                      'func_clean_pre': None,      #python function to call before other cleaning ops.   Optional.
                      'func_clean_post': None,      #python function to call after other cleaning ops.   Optional.
                      'func_compare':fuzz.ratio}    #pythion function to use for comparison.   Must have syntax F(x,y)

#Note that comparisions default to simple Levenshtein  ratio (fuzz.ratio)
# Other comparison algorithms can be tested by overriding column settings in __COL_META

#information and rules about how to handle each column in raw and derived data.
__COL_META = {
    'id':      {'ignore': True},
    'user_id': {'ignore': True},
    'first_name': {'weight': 2},   #weight is lower. First names could be nicknames.  TODO consider dictionary with common nicknames.
    'last_name':  {'weight': 5, 'func_compare':fuzz.partial_ratio}, #use partial_ratio() to smooth out puncuation in last names.
    'company':    {'weight': 1},
    'email':      {'weight': 8},
    'email_root': {'weight': 3, 'ignore':True}, #placeholder for future improvement
    'address1':   {'weight': 4},
    'address2':   {'weight': 1},
    'zip':        {'weight': 3, 'default_val':'00000', 'func_clean_pre':force_padded_zip},
    'city':       {'weight': 2},
    'state':      {'weight': 3, 'default_val': 'XX'},
    'state_long': {'weight': 1},
    'phone':      {'weight': 10,  'func_clean_pre': numeric_only},
}
```
# Usage - how to run it

A. Create and activate a virtual env (optional) 

```
python3 -m venv fuzzytest
source fuzzytest/bin/activate
```
B. install from project's requirements.txt
  ```
cd <into fuzzydupes project dir>
pip install -r requirements.txt
```

C. Run fuzzy_main to do comparison and generate JSON files
```
python fuzzy_main.py
```
This will do comparison for both normal and advanced 
the JSON results end up in the directory named output. 


D. Run flask_main to start web server
```
python flask_main.py
```

This starts flask server on port 8081.  

```
 * Serving Flask app "flask_main" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://0.0.0.0:8081/ (Press CTRL+C to quit)
```

Leave this running. 

E. go to web page to see results. 
 * In web browser, go to page:  [http://0.0.0.0:8081/]
 * Click on links at top to see results. 

##Notes:
 * Uses first user entry found as the templar for a duplicate set. 
   *All other users are marked as duplicates.   Thier correlation score displayed. 
 
 
 Things to test:
 * What happens if there are three users and if A ~= B and B ~= C, but A not ~=C.  

## addresses (city component) - built, but not used. 

Notes
* Only US addresses are considered
* Cities are first corrected to be close to a US city.
* Exact matches have a 1.0 confidence level. 
* I choose to use Levenshtein for city names as I beleive people would know how to spell thier own city names.   Any errors would be from typos, rather then guesses.  
* City names can be converted if they are close. 
* the USPS has a REST API to check and validate adresses, but is expressily forbiden to be used for data cleansing.  [https://www.usps.com/business/web-tools-apis/address-information-api.htm#_Toc487629495]
* us postal data (csv) is free from: [https://www.aggdata.com/free/united-states-zip-codes]

Cases:
 * [X] Exact match
 * [X] Zip without leading padding
 * [x] City (missspelled, zip & state OK) - result corrected. 
 * [] City & state code OK, but Zip is bad.    TODO - correct zip. 