#Main code to compare sample csv.
#
# See __main__ at bottom of file.
#
#   To understand, start with process() method


import logging
import json

import pandas as pd
from timeit import default_timer as timer
from common import *
import time
from compare_rules import *




def load(csv_path):
    """
    Load and prepare raw csv file into pandas dataframe
    :param csv_path:
    :return:  dataframe
    """

    #TODO prototype - let all exceptions fly and get returned to caller.
    #load default data from csv
    df = pd.read_csv(csv_path)

    #Create a  column named 'row_id' with unique ids.
    #row_id is used just so we have a unique way to refer to each row during debug.
    #id column is not unique.
    df['row_id'] = range(0, len(df))

    logging.info("loaded [{n}] users".format(n=len(df)))

    return df


def clean(df, all_meta):
    """Basic cleaning of data set to allow comparison later on
      The cleaning is done according to rules in all_meta.
    """


    logging.debug('clean - top')


    for col in df.columns:
        logging.debug("col[{}] - start".format(col))

        meta = all_meta.get(col)
        if not meta:
            continue   #if we didn't find meta...

        if meta.get('ignore') == True:
            continue   #explicity ignore in comparison

        #apply defaults
        df[col] = df[col].fillna(meta['default_val'])
        df[col].replace('', meta['default_val'])

        #is there a pre fix function?
        # Note - we run this before any strip or lower case.
        func = meta.get('func_clean_pre')
        if func:
            df[col] = df[col].apply(lambda x: (func)(x))

        if meta.get('whitespace_significant') == False:
            df[col] = df[col].apply(lambda x: x.strip())

        if meta.get('case_sensitive') == False:
            df[col] = df[col].apply(lambda x: x.lower())


        #force all cols to string
        #all to lower case and remove white space
        #df[col] = df[col].apply(lambda x: x.lower().strip() )


    logging.debug('clean - bottom')


def find_all_dupes(df, col_meta, correlation_floor=0.8):
    # basic algorithm
    #   n-squared at first pass.
    #        every row x every row x every col.
    #
    # For every_row in dataframe
    #   compare to every other row - get corralation x weight
    # record every row that is a likely dupe of every other.

    # repeat - this first pass is ugly n squared.
    dupes = {}

    logging.debug('find_all_dupes - top')

    # TODO NOTE:   row operations like this are slow in pandas.
    #  fix post prototype.

    df.apply(lambda user:
             find_dupes_one_user(user, df,col_meta, dupes,correlation_floor), axis=1)

    logging.debug('find_all_dupes - bottom')
    return dupes

def find_dupes_one_user(user, df,col_meta, dupes, correlation_floor):
    #basic algorithm:
    #  # compare the user (row) against all others (in df)
    #  if correlation is >= collelation_floor then mark as a dupe.

    #  Ugly n-squared at first pass.
    #        every row x every row x every col.

    #TODO - this is fast to code, but row operations are slow in pandas.
    df.apply(lambda row:  compare_users(user, row, col_meta, dupes,correlation_floor), axis=1)

def compare_users(user1, user2, col_meta, dupes,corelation_floor):
    """
    Compare two users.   If their correlation is within the boundary set by correlation_floor then
    the users are considered duplicate and added to the dupes structure.
    :param user1:
    :param user2:
    :param col_meta:
    :param dupes:
    :param corelation_floor:
    """

    tot_correlation = 0
    tot_weight = 0

    #don't compare against ourselves.
    if user1['row_id'] == user2['row_id']:
        return

    #for each column get a correlation score.
    for k, v in col_meta.items():

        meta = col_meta[k]

        if meta.get('ignore') == True:
            continue   #explicity ignore in comparison

        dbg_x = user1[k]
        dbg_y = user2[k]
        func_compare = meta['func_compare']
        correlation = (func_compare)(user1[k], user2[k])
        weighted_correlation = correlation * meta['weight']


        #note - we add the weightded correlation score to totals.  We
        # also track total weight (max weight.)
        tot_correlation += weighted_correlation
        tot_weight      += meta['weight']

    #process all column  results
    #Normalize to 0 - 1.0
    correlation = tot_correlation / (100.0 * tot_weight)

    if correlation >= corelation_floor:
        add_dupe(dupes, user1, user2, correlation)
    dbg = 13


def add_dupe(dupes, user1, user2, correlation):

    """Add unique duplicate pair by row_id to dictionary.

    if the pair already exists it will be ignored.
    """

    #ugly to reuse var name.
    #TODO - fix after prototype.

    user1 = user1.to_dict()
    user2 = user2.to_dict()

    u1 = user1['row_id']
    u2 = user2['row_id']

    #Is user2 already started as head of duplicate group?
    dupe_grp = dupes.get(u2)
    if dupe_grp:
        #add u1 as duplicate
        user1['correlation'] =  correlation
        dupe_grp[u1] = user1

    else:

        dupe_grp = dupes.get(u1)
        #create it if it doesn't exist.
        if not dupe_grp:
            dupes[u1] = {}
            dupe_grp = dupes.get(u1)
            # add this user as first match with a 1.0 correlation
            user1['correlation'] = 1.0
            dupe_grp[u1] = user1
        #add u2 as duplicate to u1
        user2['correlation'] =  correlation
        dupe_grp[u2] = user2



def get_dupe_ids(dupes):
    """
    Return a list of all row_ids of users that are listed in duplicates.
    :param dupes:
    :return: set of row_ids (ints)
    """

    ids = set()
    for k, v in dupes.items():
        ids.add(k)

        for k_sub, v_sub in v.items():
            ids.add(k_sub)
    return ids



def process(csv_path, correlation_floor = 0.8):
    """
    Main method for finding dupes.    Loads, processes, & converts output to JSON
    :param csv_path: Full path to CSV file containing users.
    :param correlation_floor: Float between 0 and 1 (inclusive)
    :return: JSON representation of dupes.
    """

    tStart = timer()

    col_meta = get_column_rules()

    #1. Load
    df = load(csv_path)


    #2. Clean
    clean(df, col_meta)

    #3. Correct & Fixup
    #Note - I built some logic to fixup incomplete addresses
    # It worked, but it didn't seem to make any difference in the results.
    #  maybe the test daata was to good.
    # Look in main of address_util.py for how it works.


    #Compare
    dupes = find_all_dupes(df, col_meta, correlation_floor=correlation_floor)
    tEnd = timer()

    logging.info('After find_all_dupes.   Found [{}] dupes with correlation_floor[{}] time[{}]'.format(len(dupes), correlation_floor, tEnd-tStart))

    #
    #build final dictionary
    #

    # Non-dupes:  get all users in dataframe not  (The ~ tilda is the boolean not clause)
    dupe_ids = get_dupe_ids(dupes)
    df_non_dupes = df[~df['row_id'].isin(dupe_ids)]

    d = {'duplicates': flatten_dupes(dupes),
        'non-duplicates': df_non_dupes.to_dict('records')
        }
    return d


def flatten_dupes(dupes):
    """
    Helper utility to make duplicate dictionary a little easier to parse
    :param dupes:
    :return:
    """

    out = []

    for k,v in dupes.items():

        is_first = True

        item = OrderedDict()
        item['user'] = None;    #we just want it first
        dupe_items = []
        item['dupe_users'] = dupe_items
        out.append(item)

        for k_sub,v_sub in v.items():

            if is_first:
                item['user'] =  v_sub
                is_first = False
            else:
                dupe_items.append(v_sub)

    return out

def results_to_str(res_dict):
    """
    Format string output for results
    :param res_dict:
    :return:  formatted string

    TODO prototype technote - this builds up final outout by appending strings. Not very efficent
    """

    #Duplicates
    s_out = "Duplicates\n"
    d = res_dict['duplicates']
    for k,v in d.items():

        s = ""
        is_first = True
        for k_sub,v_sub in v.items():

            if is_first:
                is_first = False
                s+=  ' *  '
            else:
                s += '    '  #subordinate duplicates are indented, but line up with above.
            s += "{}\n".format(v_sub)

        s_out += s;


    #Non-Duplicates
    s_out += "Non-Duplicates\n"
    d = res_dict['non-duplicates']

    for v in d:
        s_out +=  ' *  {}\n'.format(v)

    return s_out






if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    for name, csv_path in DATA_FILE_MAP.items():
        res_dict = process(csv_path, correlation_floor=CORRELATION_FLOOR)
        res_dict['summary'] = {'data': name, 'correlation_floor': CORRELATION_FLOOR, 'generated':time.strftime('%Y-%b-%d %l:%M %p %Z'),}

        j = json.dumps(res_dict, indent=0)

        #write or overwrite JSON
        fname = JSON_FILE_PATH_FORMAT.format(name=name)
        f = open(fname, "w")
        f.write(j)
        f.close()

        logging.info('Wrote JSON for data[{}] to  file path[{}]'.format(name, fname))


